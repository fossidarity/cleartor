# cleartor

[![Crates.io](https://img.shields.io/crates/v/cleartor.svg)](https://crates.io/crates/cleartor)

A simple reverse-proxy to forward clearnet traffic to an onion URL.

The use case for this is to host a server behind a firewall that's only exposed with an onion address and hosting a (anonymous) lightweight VPS that forwards all traffic received on a regular DNS address to the server.

It's similar to web2tor except it only forwards to 1 specific onion link.

`cleartor` doesn't log anything except when run in debug mode.

## Install

You need at least `libc`, `pkg-config` and `openssl` to build, and `tor` to run

```bash
sudo apt install build-essential pkg-config libssl-dev tor
```

To install cleartor just run

```bash
cargo install cleartor
```

The following should be added to `/etc/tor/torrc`

```cfg
# The port cleartor will bind on
SocksPort 9050
# Only allow local traffic
SocksPolicy accept 127.0.0.1/16
SocksPolicy reject *
```

Restart the Tor service after changing the config

```bash
sudo service tor restart
```

## Usage

Using the config in `/etc/cleartor.toml`

```bash
./cleartor
```

Using another config

```bash
./cleartor -c other-location/cleartor.toml
```

Run it on the background and never quit

```bash
nohup cleartor > /dev/null 2>&1 &
```

Run it as a non-root user and still be able to bind to port 80

```bash
sudo apt install libcap2-bin
sudo setcap 'cap_net_bind_service=+ep' ~/.cargo/bin/cleartor
```

## Example Config

`/etc/cleartor.toml`:

```toml
# The address and port to listen on
# 0.0.0.0 to listen to all connections
# 127.0.0.1 to listen to local only connections
listen_ip = "0.0.0.0"
# Set the port to 0 to receive a port from the OS
listen_port = 80
# The socks port as defined as 'SocksPort' in /etc/tor/torrc, without the address
tor_socks_port = 9050

# This section is optional
[log]
# Where to save the log
file = "/var/log/cleartor.log"
# What to log
file_level = "DEBUG"
# What to print to the console
print_level = "INFO"

[[routes]]
# The LOCATION HTTP header will be replaced with this instead of the .onion URL when you receive a response
# This field is probably misconfigured if forms aren't working
domain = "torproject.org"
# The onion address to forward all traffic to
onion = "expyuzz4wqqyqhjn.onion"

[[routes]]
# Alternatively you can also add 127.0.0.1 here to route traffic from your machine for testing purposes
domain = "127.0.0.1"
onion = "expyuzz4wqqyqhjn.onion"
```

Run with

```bash
sudo cleartor -c /etc/cleartor.toml
```

## License

Licensed under AGPLv3.
