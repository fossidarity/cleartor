/*
   cleartor
   Copyright (C) 2019

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU Affero General Public License as
   published by the Free Software Foundation, either version 3 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Affero General Public License for more details.

   You should have received a copy of the GNU Affero General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
   */

extern crate hyper;
#[macro_use]
extern crate log;
extern crate toml;

use hyper::{Body, Client, Server};
use hyper::service::service_fn;
use hyper::rt::{self, Future};
use hyper::header;
use hyper::http::uri::PathAndQuery;
use hyper_socks2::Proxy;
use simplelog::*;
use serde::Deserialize;

use std::net::SocketAddr;
use std::fs;

#[derive(Debug, Deserialize, Clone)]
#[serde(default)]
struct Config {
    listen_ip: String,
    listen_port: u16,
    tor_socks_port: u16,
    routes: Vec<RouteConfig>,
    log: LogConfig
}

impl Default for Config {
    fn default() -> Config {
        Config {
            listen_ip: "0.0.0.0".to_owned(),
            listen_port: 80,
            tor_socks_port: 9050,
            routes: Vec::new(),
            log: LogConfig::default()
        }
    }
}

#[derive(Debug, Deserialize, Clone)]
struct RouteConfig {
    domain: String,
    onion: String
}

#[derive(Debug, Deserialize, Clone)]
#[serde(default)]
struct LogConfig {
    file: String,
    file_level: String,
    print_level: String,
}

impl Default for LogConfig {
    fn default() -> LogConfig {
        LogConfig {
            file: "cleartor.log".to_owned(),
            file_level: "INFO".to_owned(),
            print_level: "INFO".to_owned(),
        }
    }
}

fn main() -> Result<(), Box<std::error::Error>> {
    let matches = clap::App::new("cleartor")
        .version("0.1")
        .about("Reverse proxy for hosting a clearnet URL for a hidden service")
        .arg(clap::Arg::with_name("config")
             .short("c")
             .long("config")
             .value_name("PATH")
             .takes_value(true))
        .get_matches();

    let config_path = matches.value_of("config").unwrap_or("cleartor.toml");

    let config_str = fs::read_to_string(config_path).expect("Could not read configuration file");
    let config: Config = toml::from_str(&*config_str).unwrap();

    // Get the log level
    let log_level = match config.log.file_level.as_ref() {
        "TRACE" => LevelFilter::Trace,
        "DEBUG" => LevelFilter::Debug,
        "INFO" => LevelFilter::Info,
        "WARN" => LevelFilter::Warn,
        "ERROR" => LevelFilter::Error,
        // Default filter
        _ => LevelFilter::Off
    };

    // Get the log level
    let print_level = match config.log.print_level.as_ref() {
        "TRACE" => LevelFilter::Trace,
        "DEBUG" => LevelFilter::Debug,
        "INFO" => LevelFilter::Info,
        "WARN" => LevelFilter::Warn,
        "ERROR" => LevelFilter::Error,
        // Default filter
        _ => LevelFilter::Off
    };

    // Create the loggers
    CombinedLogger::init(
        vec![
        TermLogger::new(print_level, simplelog::Config::default()).unwrap(),
        WriteLogger::new(log_level, simplelog::Config::default(), fs::File::create(config.log.file.clone())?),
        ]
    )?;

    let listen_address = SocketAddr::new(config.listen_ip.parse()?, config.listen_port);
    info!("Listening for incoming messages on: {}", listen_address);

    let proxy_address = format!("127.0.0.1:{}", config.tor_socks_port);
    info!("Binding to Tor proxy on: {}", proxy_address);

    let proxy = Proxy::Socks5 {
        addrs: proxy_address,
        auth: None
    };

    let client = Client::builder()
        .keep_alive(true)
        .build::<_, Body>(proxy);

    // Then forward the data to the Tor server
    // new_service is run for each connection, creating a 'service'
    // to handle requests for that specific connection
    let new_service = move || {
        let client = client.clone();
        let config = config.clone();

        service_fn(move |mut req| {
            debug!("From {:?}", req);

            let host: &str = req.headers()
                .get(header::HOST).unwrap()
                .to_str().unwrap();

            // Find the host
            let route = config.routes.iter()
                .find(|&route| host.contains(&*route.domain))
                .unwrap()
                .clone();

            // Change the URL to the Tor URL
            let uri_string = format!("http://{}{}", route.onion, req.uri()
                                     .path_and_query()
                                     .unwrap_or(&PathAndQuery::from_static("/")).as_str());
            debug!("Request to {}", uri_string);
            let uri = uri_string.parse().unwrap();
            *req.uri_mut() = uri;

            // Remove the HOST header because it contains the wrong address
            req.headers_mut().remove(header::HOST);

            debug!("To {:?}", req);

            // Then await the result
            let new_req = client.request(req);
            new_req.and_then(move |mut res| {
                // Replace the Tor address in the location URL with the clearnet one
                let headers = res.headers_mut();
                if let Some(location) = headers.get(header::LOCATION) {
                    let location_str = location.to_str().unwrap();
                    headers.insert(header::LOCATION, location_str.replace(&*route.onion, &*route.domain).parse().unwrap());
                }

                Ok(res)
            })
        })
    };

    // Run the server
    let server = Server::bind(&listen_address)
        .serve(new_service)
        .map_err(|e| error!("server error: {}", e));

    rt::run(server);

    Ok(())
}
